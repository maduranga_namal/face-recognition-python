# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 20:08:21 2021

@author: Maduranga
"""

import cv2 as cv

face_detector = cv.CascadeClassifier('filters & classifiers/haarcascade_frontalface_default.xml')
eye_detector = cv. CascadeClassifier('filters & classifiers/haarcascade_eye.xml')

img = cv.imread('Images/Angelina_Jolie/Angelina_Jolie_0020.jpg',1)
gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)


while (True):
    faces = face_detector.detectMultiScale(gray_img, 1.3, 2)
    # print(faces)
    for(x,y,w,h) in faces:
        cv.rectangle(img,(x,y), (x+w, y+h), (255,255,0), 2)
        # print(x,y,x+w,y+h)
        regionOfInterest_grayimg = gray_img[y:y+h, x:x+w]
        regionOfInterest_colorimg = img[y:y+h, x:x+w]

        eyes = eye_detector.detectMultiScale(regionOfInterest_grayimg)
        print(eyes)
        for(ex,ey,ew,eh) in eyes:
            cv.rectangle(regionOfInterest_colorimg,(ex,ey), (ex+ew, ey+eh), (0,255,0), 1)
            
    break
 
cv.imshow("Eyes detected !!",img)
cv.waitKey(0)
cv.destroyAllWindows()
    
