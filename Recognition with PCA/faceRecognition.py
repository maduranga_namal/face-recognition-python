# -*- coding: utf-8 -*-
"""
Created on Sat Sep  4 08:48:42 2021

@author: Maduranga
"""

import cv2 as cv
from algoClass import my_algo
from imageMatrix import imageToMatrixClass
from dataset import datasetClass

reco_type = "image";

no_of_images_of_one_person = 508
dataset_obj = datasetClass(no_of_images_of_one_person)

images_paths_for_training = dataset_obj.images_path_for_training
labels_for_training = dataset_obj.labels_for_training
no_of_elements_for_training = dataset_obj.no_of_images_for_training

images_paths_for_testing = dataset_obj.images_path_for_testing
labels_for_testing = dataset_obj.labels_for_testing
no_of_elements_for_testing = dataset_obj.no_of_images_for_testing

images_targets = dataset_obj.images_target

img_width, img_height = 50, 50

imageToMatrixClassObj = imageToMatrixClass(images_paths_for_training, img_width, img_height)
img_matrix = imageToMatrixClassObj.get_matrix()

my_algo_class_obj = my_algo(img_matrix, labels_for_training, images_targets, no_of_elements_for_training, img_width, img_height, quality_percentage=90)
new_coordinates = my_algo_class_obj.reduce_dim()

if reco_type == "image":
    correct = 0
    wrong = 0
    i = 0
    
    for img_path in images_paths_for_testing:
        img = my_algo_class_obj.img_from_path(img_path)
        my_algo_class_obj.show_image("Recognize Image", img)
        new_cords_for_image = my_algo_class_obj.new_cords(img)
        
        finded_name = my_algo_class_obj.recognize_face(new_cords_for_image)
        target_index = labels_for_testing[i]
        original_name = images_targets[target_index]
        
        if finded_name is original_name:
            correct +=1
            print("Correct Result", " Name:", finded_name)
            
        else:
            wrong +=1

            print("Wrong Result", " Name:", finded_name)
            
        i += 1
        
        
    print("Total Correct", correct)
    print("Total wrong", wrong)
    print("Percentage", correct/(correct + wrong) *100)
    cv.destroyAllWindows()
    



