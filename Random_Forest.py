# -*- coding: utf-8 -*-
"""
Created on Sun Sep 26 02:00:09 2021

@author: Maduranga
"""

import numpy as np
import matplotlib.pyplot as plt
import glob
import cv2 as cv
import os
import pandas as pd


train_images = []
test_images = []
train_labels = []
test_labels = []


dir = "Image_writes/"

for name in os.listdir(dir):
    img_count = 0
    dir_path = os.path.join(dir, name)
    if os.path.isdir(dir_path):
        print()
        for img_name in os.listdir(dir_path):
            img_path = os.path.join(dir_path, img_name)
            img = cv.imread(img_path)
            img_count +=1
            if(img_count < len(os.listdir(dir_path))*(80/100)):
                train_images.append(img)
                train_labels.append(name)
                
            else:
                test_images.append(img)
                test_labels.append(name)
                
train_images = np.array(train_images)
train_labels = np.array(train_labels)
test_images = np.array(test_images)
test_labels = np.array(test_labels)
             
#Encode labels from text to integers

from sklearn import preprocessing
le = preprocessing.LabelEncoder()
le.fit(test_labels)
test_labels_encoded = le.transform(test_labels)
le.fit(train_labels)
train_labels_encoded = le.transform(train_labels)

x_train, y_train, x_test, y_test = train_images, train_labels_encoded, test_images, test_labels_encoded 

x_train, x_test = x_train / 255.0, x_test / 255.0           
        
        
#define classifier RF
from sklearn.ensemble import RandomForestClassifier
RF_model = RandomForestClassifier(n_estimators = 50, random_state=42)


image_features = np.expand_dims(x_train, axis=0)
X_for_RF = np.reshape(image_features, (x_train.shape[0], -1))

test_features = np.expand_dims(x_test, axis=0)
test_for_RF = np.reshape(test_features, (x_test.shape[0], -1))

RF_model.fit(X_for_RF, y_train)

#predict on test
test_prediction = RF_model.predict(test_for_RF)

#inverse le transform to get original label back
test_prediction = le.inverse_transform(test_prediction)

#print accuracy
from sklearn import metrics
print('Accuracy:',metrics.accuracy_score(test_labels, test_prediction))

   

  
        
