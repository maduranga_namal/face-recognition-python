# -*- coding: utf-8 -*-
"""
Created on Tue Aug 24 21:32:44 2021

@author: Maduranga
"""

import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import pandas as pd
import os

path = 'D:/Sabaragamuwa/AAA Year 4 sem 1/Research Materials/Algorithms and Codes n video/Images/test' 
write_path = 'D:/Sabaragamuwa/AAA Year 4 sem 1/Research Materials/Algorithms and Codes n video/Images/csv_files'
  
os.chdir(path)        
lists = os.listdir(path)    
labels = []
file_list = []
data = []

df = pd.DataFrame()

for file in lists:
    labels.append(file)
    file_path = path + "/" + file
    # data.append( cv.imread(file_path, cv.COLOR_BGR2GRAY))
    img = cv.imread(file_path)
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    img_reshaped = img.reshape(-1)
    df[file] = img_reshaped
    
print(df.head)   
os.chdir(write_path) 
df.to_csv('Imagedata.csv')   