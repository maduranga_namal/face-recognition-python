# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 16:28:17 2021

@author: Maduranga
"""

import os
import cv2 as cv


height = 150
width = 150
dim = (height, width)

dir = "fifty_persons_lfw/"

for name in os.listdir(dir):
    
    img_count = 0
    dir_path = os.path.join(dir, name)  #name => name of person
    if os.path.isdir(dir_path):
        
        os.mkdir('fifty_spilt/train/'+name+'')
        os.mkdir('fifty_spilt/test/'+name+'')
        
        for img_name in os.listdir(dir_path):
            img_count +=1
            img_path = os.path.join(dir_path, img_name) #get image path
            img = cv.imread(img_path)
            
            img_final = cv.resize(img, dim, interpolation = cv.INTER_AREA)
            
            if(img_count < len(os.listdir(dir_path))*(90/100)):
                cv.imwrite('fifty_spilt/train/'+name+'/'+img_name, img_final)
                
            else:
                cv.imwrite('fifty_spilt/test/'+name+'/'+img_name, img_final)
                
            
            