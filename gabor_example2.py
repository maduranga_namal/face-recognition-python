# -*- coding: utf-8 -*-
"""
Created on Sat Aug 21 22:55:58 2021

@author: Maduranga
"""

import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import pandas as pd

img = cv.imread('Images/Angelina_Jolie/Angelina_Jolie_0007.jpg')
img = cv.cvtColor(img, cv.COLOR_BGR2GRAY) 

ksize = 5
sigma = 3
theta = 1*np.pi/4
lamda = 1*np.pi/4
gamma = 0.4
phi = 0

kernel = cv.getGaborKernel((ksize, ksize), sigma, theta, lamda, gamma, phi, ktype=cv.CV_32F)
fimg = cv.filter2D(img, cv.CV_8UC3, kernel)

kernel_resized = cv.resize(kernel, (400, 400))
cv.imshow('kernel', kernel_resized)
cv.imshow('Original Image', img)
cv.imshow('Filtered Image', fimg)
cv.waitKey()
cv.destroyAllWindows()