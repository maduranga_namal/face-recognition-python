# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 14:35:27 2021

@author: Maduranga
"""

import os
import numpy as np
import cv2 as cv
import pandas as pd

dir = "images/"
df = pd.DataFrame()
kernels = []
height = 200
width = 200
dim = (height, width)

face_detect = cv.CascadeClassifier("classifiers/haarcascade_frontalface_default.xml")


#cropped = image[startY:endY, startX:endX]

for name in os.listdir(dir):
    os.mkdir('Image_writes/'+name+'')
    num = 1
    
    dir_path = os.path.join(dir, name)  #name => name of person
    if os.path.isdir(dir_path):
        for img_name in os.listdir(dir_path):
            #print (img_name)
            
            img_path = os.path.join(dir_path, img_name) #get image path
            img = cv.imread(img_path)
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            face = face_detect.detectMultiScale(img, 1.3, 1)
            
            for (x,y,w,h) in face:
                cropped_img = img[y:y+h, x:x+w]
                img_reshaped = cropped_img.reshape(-1)
            
                for theta in range(4):
                    theta = theta / 4. * np.pi
                    for sigma in (1, 3):
                        for lamda in np.arange(0, np.pi, np.pi /4):
                            for gamma in (0.05, 0.5):
                            
                                gabor_label = 'Gabor' + str(num)
                            
                                ksize = 5
                                phi = 0
                                kernel = cv.getGaborKernel((ksize,ksize), sigma, theta, lamda, gamma, phi, ktype=cv.CV_32F)
                            
                                kernels.append(kernel)
                                fimg = cv.filter2D(img_reshaped, cv.CV_8UC3, kernel)
                                filtered_img  = fimg.reshape(-1)
                                img_final = filtered_img.reshape(cropped_img.shape)
                                
                                img_final = cv.resize(img_final, dim, interpolation = cv.INTER_AREA)
                                if cv.countNonZero(img_final) != 0:
                                    cv.imwrite('Image_writes/'+name+'/'+gabor_label+'.jpg', img_final)
                                    num +=1
                            
                                print(gabor_label, ': theta=', theta, ': sigma=',sigma, ': lamda=',lamda, ': gamma=',gamma)
              
                            
                       