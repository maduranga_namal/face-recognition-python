# -*- coding: utf-8 -*-
"""
Created on Fri Oct  1 01:55:40 2021

@author: Maduranga
"""

import os

dir = "fifty_ten_persons_lfw/"
no_of_images = 10


for name in os.listdir(dir):
    dir_path = os.path.join(dir, name)
    if os.path.isdir(dir_path):
        if len(os.listdir(dir_path)) >= no_of_images:
            print(name)
            count = 0
            for filename in os.listdir(dir_path):
                filepath = os.path.join(dir_path, filename)
                print(filename)
                count +=1
                if count > no_of_images:
                    os.remove(filepath)
                    
                
        
   