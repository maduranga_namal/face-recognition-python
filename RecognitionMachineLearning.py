# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 17:57:56 2021

@author: Maduranga
"""

import numpy as np
import matplotlib.pyplot as plt
import glob
import cv2 as cv
import os
import seaborn as sns
import pandas as pd
from skimage.filters import sobel

#######################################################

train_images = []
train_labels = []

for dir_path in glob.glob("train/*"):
    label = dir_path.split("\\")[-1]
    print(label)
    for img_path in glob.glob(os.path.join(dir_path, "*.jpg")):
        # print(img_path)
        img = cv.imread(img_path, cv.IMREAD_COLOR)
        
        train_images.append(img)
        train_labels.append(label)
        
train_images = np.array(train_images)
train_labels = np.array(train_labels)

#######################################################

test_images = []
test_labels = []

for dir_path in glob.glob("test/*"):
    label = dir_path.split("\\")[-1]
    # print(label)
    for img_path in glob.glob(os.path.join(dir_path, "*.jpg")):
        # print(img_path)
        img = cv.imread(img_path, cv.IMREAD_COLOR)
        
        test_images.append(img)
        test_labels.append(label)
        
test_images = np.array(test_images)
test_labels = np.array(test_labels)

# print(train_images)

#######################################################
#Encode labels from text to integers

from sklearn import preprocessing
le = preprocessing.LabelEncoder()
le.fit(test_labels)
test_labels_encoded = le.transform(test_labels)
le.fit(train_labels)
train_labels_encoded = le.transform(train_labels)

x_train, y_train, x_test, y_test = train_images, train_labels_encoded, test_images, test_labels_encoded

x_train, x_test = x_train / 255.0, x_test / 255.0

#######################################################
#input shape is (n, x, y, c) - no of images, x dimension, y , c- no of channels 
def feature_extractor(dataset):
    x_train = dataset
    image_dataset = pd.DataFrame()
    for image in range(x_train.shape[0]):
        # print(image)
        
        df = pd.DataFrame()
        
        input_img = x_train[image, :,:,:]
        img = input_img
      ##################################################
        # Adding data to the dataframe
      
      
        # Add pixel values to data frame
        pixel_values = img.reshape(-1)
        df['Pixel_Value'] = pixel_values
        
        
        num = 1
        kernels = []
        for theta in range(2):
            theta = theta / 4. * np.pi
            for sigma in (1, 3):
                lamda = np.pi/4
                gamma = 0.5
                gabor_label = 'Gabor' +str(num)
                
                ksize = 9
                kernel = cv.getGaborKernel((ksize, ksize), sigma, theta, lamda, gamma, 0, ktype=cv.CV_32F )
                kernels.append(kernel)
                
                fimg = cv.filter2D(img, cv.CV_8UC3, kernel)
                filtered_img = fimg.reshape(-1) 
                df[gabor_label] = filtered_img
                num += 1
        
        #Sobel filter(good for edge detection)
        # edge_sobel = sobel(img)
        # edge_sobel1 = edge_sobel.reshape(-1)
        # df['Sobel'] = edge_sobel1
        
        image_dataset = image_dataset.append(df)
        
    return image_dataset

##########################################################################

#features form training images
image_features = feature_extractor(x_train)

#reshape vector for classification
n_features = image_features.shape[1]
image_features = np.expand_dims(image_features, axis=0)
X_for_RF = np.reshape(image_features, (x_train.shape[0], -1))

#define classifier
from sklearn.ensemble import RandomForestClassifier
RF_model = RandomForestClassifier(n_estimators = 50, random_state=42)
RF_model.fit(X_for_RF, y_train)

#can also use SVM but RF is faster
# from sklearn import svm
# SVM_model = svm.SVC(decision_function_shape='ovo') # for multiclass classification
# SVM_model.fit(X_for_RF, y_train)


#predict test data
#extract features from them and reshape
test_features = feature_extractor(x_test)
test_features = np.expand_dims(test_features, axis=0)
test_for_RF = np.reshape(test_features, (x_test.shape[0], -1))

#predict on test
test_prediction = RF_model.predict(test_for_RF)

#inverse le transform to get original label back
test_prediction = le.inverse_transform(test_prediction)

#print accuracy
from sklearn import metrics
print('Accuracy:',metrics.accuracy_score(test_labels, test_prediction))

#print confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(test_labels, test_prediction)

fig, ax = plt.subplots(figsize=(6,6))
sns.set(font_scale=1.6)
sns.heatmap(cm, annot=True, ax = ax) 


#check results on a few random images

import random
n = random.randint(0, x_test.shape[0]-1)
img = x_test[n]
plt.imshow(img)

#extract features and reshape to right dimensions
input_img = np.expand_dims(img, axis=0)
input_img_features = feature_extractor(input_img)
input_img_features = np.expand_dims(input_img_features, axis=0)
input_img_for_RF = np.reshape(input_img_features, (input_img.shape[0], -1))

#predict
img_prediction = RF_model.predict(input_img_for_RF)
img_prediction = le.inverse_transform([img_prediction])
print('Predicted label for this image is:', img_prediction)
print('Actual label for this image is:', test_labels[n])


    
    
                
                


















