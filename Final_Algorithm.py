# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 19:04:42 2021

@author: Maduranga
"""

import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn import metrics
from sklearn import preprocessing
import numpy as np
import glob
import cv2 as cv
import os
import pandas as pd



####################### Start Pre-processing and Write Gabor Filter Bank ###################################

dir = "images/"
df = pd.DataFrame()
kernels = []
height = 150
width = 150
dim = (height, width)

face_detect = cv.CascadeClassifier("classifiers/haarcascade_frontalface_default.xml") #face detector


# os.mkdir('Image_writes')

for name in os.listdir(dir):
    os.mkdir('Image_writes/'+name+'')
    num = 1
    
    dir_path = os.path.join(dir, name)  #name => name of person
    if os.path.isdir(dir_path):
        for img_name in os.listdir(dir_path):
            #print (img_name)
            
            img_path = os.path.join(dir_path, img_name) #get image path
            img = cv.imread(img_path)
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            face = face_detect.detectMultiScale(img, 1.3, 1)
            
            for (x,y,w,h) in face:
                cropped_img = img[y:y+h, x:x+w] #cropped = image[startY:endY, startX:endX] - crop face #######################
                img_reshaped = cropped_img.reshape(-1)
            
                for theta in range(4):
                    theta = theta / 4. * np.pi
                    for sigma in (1, 3):
                        for lamda in np.arange(0, np.pi, np.pi /4):
                            for gamma in (0.05, 0.5):
                            
                                gabor_label = 'Gabor' + str(num)
                            
                                ksize = 5
                                phi = 0
                                kernel = cv.getGaborKernel((ksize,ksize), sigma, theta, lamda, gamma, phi, ktype=cv.CV_32F)
                            
                                kernels.append(kernel)
                                fimg = cv.filter2D(img_reshaped, cv.CV_8UC3, kernel)
                                filtered_img  = fimg.reshape(-1)
                                img_final = filtered_img.reshape(cropped_img.shape)
                                
                                img_final = cv.resize(img_final, dim, interpolation = cv.INTER_AREA)
                                # if cv.countNonZero(img_final) != 0:
                                    
                                    ####### Remove Outliers ################   
                                if cv.countNonZero(img_final) > 2250:
                                    cv.imwrite('Image_writes/'+name+'/'+gabor_label+'.jpg', img_final)
                                    num +=1
                            
                                print(gabor_label, ': theta=', theta, ': sigma=',sigma, ': lamda=',lamda, ': gamma=',gamma)
                                
######################################### End Pre-processing and Write Gabor Filter Bank ########################################

train_images = []
test_images = []
train_labels = []
test_labels = []

train_dataset = []
test_dataset = []


dir = "Image_writes/"

for name in os.listdir(dir):
    img_count = 0
    dir_path = os.path.join(dir, name)
    if os.path.isdir(dir_path):
        print()
        for img_name in os.listdir(dir_path):
            img_path = os.path.join(dir_path, img_name)
            img = cv.imread(img_path)
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            img_count +=1
            if(img_count < len(os.listdir(dir_path))*(90/100)):
                train_images.append(img)
                train_labels.append(name)
                
            else:
                test_images.append(img)
                test_labels.append(name)
                
train_images = np.array(train_images)
train_labels = np.array(train_labels)
test_images = np.array(test_images)
test_labels = np.array(test_labels)
             
#################  Encode labels from text to integers ###########################
le = preprocessing.LabelEncoder()
le.fit(test_labels)
test_labels_encoded = le.transform(test_labels)
le.fit(train_labels)
train_labels_encoded = le.transform(train_labels)

# print(train_images.shape)
# print(test_images.shape)

n_train_samples, nx, ny = train_images.shape
train_dataset = train_images.reshape((n_train_samples,nx*ny))

n_test_samples, no_x, no_y = test_images.shape
test_dataset = test_images.reshape((n_test_samples, no_x*no_y))

print(train_dataset.shape)
print(test_dataset.shape)
           
x_train, y_train, x_test, y_test = train_dataset, train_labels_encoded, test_dataset, test_labels_encoded 
  

    
  
##################################################### Compute a PCA #####################################################
n_components = 2000
pca = PCA(n_components=n_components, whiten=True).fit(x_train)
# apply PCA transformation
X_train_pca = pca.transform(x_train)
X_test_pca = pca.transform(x_test)            
            
print(X_train_pca.shape)
print(X_test_pca.shape)



##################################### Classifier -- Random Forest ##########################################################
RF_model = RandomForestClassifier(n_estimators = 50, random_state=42)


image_features = np.expand_dims(X_train_pca, axis=0)
X_for_RF = np.reshape(image_features, (X_train_pca.shape[0], -1))

test_features = np.expand_dims(X_test_pca, axis=0)
test_for_RF = np.reshape(test_features, (X_test_pca.shape[0], -1))

RF_model.fit(X_for_RF, y_train)

################# predict on test ################
test_prediction = RF_model.predict(test_for_RF)

#### inverse le transform to get original label back ###############
test_prediction = le.inverse_transform(test_prediction)


print('Accuracy:',metrics.accuracy_score(test_labels, test_prediction))

##################################### End ##########################################################
                    
           