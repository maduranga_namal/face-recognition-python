# -*- coding: utf-8 -*-
"""
Created on Wed Jul 21 12:11:43 2021

@author: Maduranga
"""

import cv2 as cv

face_detector = cv.CascadeClassifier('filters & classifiers/haarcascade_frontalface_default.xml')


img = cv.imread('Images/Angelina_Jolie/Angelina_Jolie_0020.jpg',1)
gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)


faces = face_detector.detectMultiScale(gray_img, 1.3, 2)

# print(faces)

for(x,y,w,h) in faces:
    cv.rectangle(img,(x,y), (x+w, y+h), (255,255,0), 2)
    print(x,y,x+w,y+h)
    
cv.imshow("Face detected !",img)
cv.waitKey(0)
cv.destroyAllWindows()
