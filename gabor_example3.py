# -*- coding: utf-8 -*-
"""
Created on Sun Aug 22 00:47:27 2021

@author: Maduranga
"""

import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import pandas as pd

img = cv.imread('Images/test/David_Beckham_0002.jpg')
img = cv.cvtColor(img, cv.COLOR_BGR2GRAY) 

df = pd.DataFrame()
img2 = img.reshape(-1)
df['Original Pixels'] = img2

num = 1
for theta in range(2):
    theta = theta /4. *np.pi
    for sigma in (3, 5):
        for lamda in np.arange(0, np.pi, np.pi /4.):
            for gamma in (0.05, 0.5):
                gabor_label = 'Gabor' + str(num)
                kernel = cv.getGaborKernel((5, 5), sigma, theta, lamda, gamma, 0, ktype=cv.CV_32F)
                fimg = cv.filter2D(img, cv.CV_8UC3, kernel)
                filtered_img = fimg.reshape(-1)
                df[gabor_label] = filtered_img
                num +=1
                
print(df.head)   
# df.to_csv('GaborExample.csv')   
            
      
        
      

# ksize = 5
# sigma = 3
# theta = 1*np.pi/4
# lamda = 1*np.pi/4
# gamma = 0.4
# phi = 0

# kernel = cv.getGaborKernel((ksize, ksize), sigma, theta, lamda, gamma, phi, ktype=cv.CV_32F)
# fimg = cv.filter2D(img, cv.CV_8UC3, kernel)

# kernel_resized = cv.resize(kernel, (400, 400))
# cv.imshow('kernel', kernel_resized)
# cv.imshow('Original Image', img)
# cv.imshow('Filtered Image', fimg)
# cv.waitKey()
# cv.destroyAllWindows()